﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Beadando2
{
	/// <summary>
	/// Küszöbkör osztály
	/// </summary>
	public class Küszöbkör : Jatekelem
	{
		/// <summary>
		/// sugár érték
		/// </summary>
		private double r;

		/// <summary>
		/// Initializes a new instance of the <see cref="Küszöbkör"/> class.
		/// </summary>
		/// <param name="aw">Actual Width</param>
		/// <param name="ah">Actual Heigth</param>
		/// <param name="sugar">sugár érték</param>
		public Küszöbkör(double aw, double ah, double sugar)
		{
			KözepPont = new Point(aw / 2.0, ah / 2.0);
			G = new EllipseGeometry(new Point(0, 0), sugar, sugar);
			r = sugar;
		}

		/// <summary>
		/// Gets or sets r
		/// </summary>
		public double R
		{
			get
			{
				return r;
			}

			set
			{
				r = value;
			}
		}
	}
}
