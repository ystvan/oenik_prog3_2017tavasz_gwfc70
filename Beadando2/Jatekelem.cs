﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Beadando2
{
	/// <summary>
	/// Abstrakt Jatekelem osztály
	/// </summary>
	public abstract class Jatekelem
	{
		/// <summary>
		/// Magasság egység
		/// </summary>
		protected const double M = 20;

		/// <summary>
		/// Fordítási szög
		/// </summary>
		private double szog;

		/// <summary>
		/// Alak geometria
		/// </summary>
		private Geometry g;

		/// <summary>
		/// Közép pont
		/// </summary>
		private Point közepPont;

		/// <summary>
		/// Gets or sets g
		/// </summary>
		public Geometry G
		{
			get
			{
				TransformGroup tg = new TransformGroup();
				tg.Children.Add(new TranslateTransform(KözepPont.X, KözepPont.Y));
				tg.Children.Add(new RotateTransform(szog, KözepPont.X, KözepPont.Y));
				g.Transform = tg;
				return g;
			}

			set
			{
				g = value;
			}
		}

		/// <summary>
		/// Gets or sets középpont
		/// </summary>
		public Point KözepPont
		{
			get
			{
				return közepPont;
			}

			set
			{
				közepPont = value;
			}
		}

		/// <summary>
		/// Gets or sets szog
		/// </summary>
		public double Szog
		{
			get
			{
				return szog;
			}

			set
			{
				szog = value;
			}
		}

		/// <summary>
		/// Ütközés vizsgálat
		/// </summary>
		/// <param name="je">Vizsgálandó elem</param>
		/// <returns>Ütközike az elemmel</returns>
		public bool Utkozik(Jatekelem je)
		{
			return Geometry.Combine(G, je.G, GeometryCombineMode.Intersect, null).GetArea() > 0;
		}
	}
}
