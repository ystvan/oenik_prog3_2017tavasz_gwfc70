﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;

namespace Beadando2
{
	/// <summary>
	/// Header Jatek
	/// </summary>
	public class Jatek : FrameworkElement
	{
		/// <summary>
		/// Szint szám
		/// </summary>
		private const int Szintszam = 4;

		/// <summary>
		/// Fal lista
		/// </summary>
		private List<Fal> falak;

		/// <summary>
		/// Zöld mező lista 
		/// </summary>
		private List<IMezo> pluszIdőMezo;

		/// <summary>
		/// Kék mező lista 
		/// </summary>
		private List<IMezo> kapuNyito;

		/// <summary>
		/// Ütemjelző lista 
		/// </summary>
		private List<UtemJelzo> utemjelzok;

		/// <summary>
		/// Játékos referencia 
		/// </summary>
		private Jatekos jtk;

		/// <summary>
		/// Cél mező
		/// </summary>
		private Fal cel;

		/// <summary>
		/// Dispecher Timer
		/// </summary>
		private DispatcherTimer dt;

		/// <summary>
		/// Üldöző kör
		/// </summary>
		private Küszöbkör kk;

		/// <summary>
		/// Stopwatch időméréshez
		/// </summary>
		private Stopwatch sw;

		/// <summary>
		/// Kiirandó  módusoló szöveg
		/// </summary>
		private FormattedText ft;

		/// <summary>
		/// Kiirandó  módusoló szöveg
		/// </summary>
		private FormattedText fmt;

		/// <summary>
		/// Értékelő érem
		/// </summary>
		private Erem em;

		/// <summary>
		/// Gomb hely
		/// </summary>
		private Rect r1;

		/// <summary>
		/// Gomb hely
		/// </summary>
		private Rect r2;

		/// <summary>
		/// Ütem értékelő
		/// </summary>
		private Geometry utemCel;

		/// <summary>
		/// Idő értékelő
		/// </summary>
		private int arany;

		/// <summary>
		/// Idő értékelő
		/// </summary>
		private int ezust;

		/// <summary>
		/// Idő értékelő
		/// </summary>
		private int bronz;

		/// <summary>
		/// Kirajzolás feltétel
		/// </summary>
		private int state;

		/// <summary>
		/// Aktuális szint
		/// </summary>
		private int szint;

		/// <summary>
		/// Initializes a new instance of the <see cref="Jatek"/> class. 
		/// </summary>
		public Jatek()
		{
			StateChange(0);
			dt = new DispatcherTimer();
			sw = new Stopwatch();
			Loaded += this.Jatek_Loaded;
		}

		/// <summary>
		/// Tick esemény
		/// </summary>
		/// <param name="sender">generált bemenet</param>
		/// <param name="e">másik generált bemenet</param>
		public void Dt_Tick(object sender, EventArgs e)
		{
			Leptet();
			InvalidateVisual();
		}

		/// <summary>
		/// Játékos Mozgás metódus
		/// </summary>
		/// <param name="irany">Előre vagy hátra</param>
		public void Mozog(bool irany)
		{
			if (jtk != null)
			{
				Point regiKP = jtk.Mozog(irany);
				bool utkozik = false;
				foreach (Fal a in falak)
				{
					if (jtk.Utkozik(a))
					{
						utkozik = true;
					}
				}

				if (utkozik)
				{
					jtk.KözepPont = regiKP;
				}

				Leptet();
				InvalidateVisual();
			}
		}

		/// <summary>
		/// Játékos Fordul metódus
		/// </summary>
		/// <param name="irany">Jobbra vagy balra</param>
		public void Fordul(bool irany)
		{
			if (jtk != null)
			{
				Point regiKP = jtk.Fordul(ActualWidth, ActualHeight, irany);
				bool utkozik = false;
				foreach (Fal a in falak)
				{
					if (jtk.Utkozik(a))
					{
						utkozik = true;
					}
				}

				if (utkozik)
				{
					jtk.KözepPont = regiKP;

					if (irany)
					{
						jtk.Szog -= 2;
					}
					else
					{
						jtk.Szog += 2;
					}
				}

				Leptet();
			}

			InvalidateVisual();
		}

		/// <summary>
		/// Jaték végét kezelő metódus
		/// </summary>
		/// <param name="siker">Nyert vagy vesztett</param>
		public void JatekVege(bool siker)
		{
			sw.Stop();
			jtk = null;
			em = null;
			if (siker)
			{
				em = new Erem(ActualWidth, ActualHeight);

				if (sw.ElapsedMilliseconds < arany)
				{
					em.Szin = 0;
				}
				else if (sw.ElapsedMilliseconds > arany && sw.ElapsedMilliseconds < ezust)
				{
					em.Szin = 1;
				}
				else if (sw.ElapsedMilliseconds > ezust && sw.ElapsedMilliseconds < bronz)
				{
					em.Szin = 2;
				}
				else
				{
					em.Szin = 3;
				}

				state = 2;

				if (szint != Szintszam)
				{
					ft = new FormattedText("Következő szint", CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, new Typeface("Tahoma"), 12, Brushes.Black);
					ft.SetFontSize(32);
				}
				else
				{
					ft = new FormattedText("Gratulálok kijátszottad a játékot", CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, new Typeface("Tahoma"), 12, Brushes.Black);
					ft.SetFontSize(32);
				}

				fmt = new FormattedText("Nyertél!\n" + (sw.ElapsedMilliseconds / 1000).ToString() + "." + (sw.ElapsedMilliseconds % 1000).ToString() + " mp", CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, new Typeface("Tahoma"), 12, Brushes.Black);
				fmt.SetFontSize(32);
			}
			else
			{
				state = 2;
				ft = new FormattedText("Újra", CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, new Typeface("Tahoma"), 12, Brushes.Black);
				ft.SetFontSize(32);
				fmt = new FormattedText("Vesztettél!", CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, new Typeface("Tahoma"), 12, Brushes.Black);
				fmt.SetFontSize(32);
			}
		}

		/// <summary>
		/// Megjelenítés feltétel váltózást kezelő metódus 
		/// </summary>
		/// <param name="szam">mire vált</param>
		public void StateChange(int szam)
		{
			state = szam;
			if (state == 1)
			{
				PalyaInit();
				sw.Restart();
			}
			else if (state == 0)
			{
				ft = new FormattedText("Start", CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, new Typeface("Tahoma"), 12, Brushes.Black);
				ft.SetFontSize(32);
				if (szint != 0)
				{
					fmt = new FormattedText("Folyatatás (" + (szint + 1) + ". szinttől)", CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, new Typeface("Tahoma"), 12, Brushes.Black);
					fmt.SetFontSize(32);
				}
			}
		}

		/// <summary>
		/// Palya betöltés
		/// </summary>
		public void PalyaInit()
		{
			utemjelzok = new List<UtemJelzo>();
			utemjelzok.Add(new UtemJelzo(ActualWidth, ActualHeight, 0));
			utemjelzok.Add(new UtemJelzo(ActualWidth, ActualHeight, -55));
			utemjelzok.Add(new UtemJelzo(ActualWidth, ActualHeight, 55));
			if (szint == 0)
			{
				arany = 50000;
				ezust = 55000;
				bronz = 100000;
				falak = new List<Fal>();
				falak.Add(new Fal(ActualWidth, ActualHeight, 1, 0, 0, true));
				falak.Add(new Fal(ActualWidth, ActualHeight, 2, 90, 270, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 2, 270, 90, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 3, 180, 0, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 3, 0, 180, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 4, 270, 90, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 4, 90, 270, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 5, 0, 120, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 5, 120, 240, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 5, 240, 360, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 6, 30, 150, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 6, 150, 30, true));
				falak.Add(new Fal(ActualWidth, ActualHeight, 7, 60, 150, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 7, 150, 240, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 7, 240, 330, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 7, 330, 60, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 8, 180, 180, true));
				falak.Add(new Fal(ActualWidth, ActualHeight, 2.5, 30, 60, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 2.5, 200, 230, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 3.5, 120, 140, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 3.5, 300, 320, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 4.5, 32, 48, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 4.5, 172, 188, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 5.5, 327, 340, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 5.5, 130, 142, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 6.5, 85, 95, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 6.5, 355, 5, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 6.5, 200, 212, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 7.5, 160, 170, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 7.5, 130, 140, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 7.5, 270, 280, false));
				pluszIdőMezo = null;
				kapuNyito = null;
				jtk = new Jatekos(ActualWidth, ActualHeight);
				cel = new Fal(ActualWidth, ActualHeight, 8, 180, 180, false);
				kk = new Küszöbkör(ActualWidth, ActualHeight, 0);
				fmt = new FormattedText("Iránytás: Up,Down,Right,Down \nMenekülj a piros kör elöl!\n Van 2 mp előnyöd.\nA cél a sárga terület.", CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, new Typeface("Tahoma"), 12, Brushes.Black);
			}
			else if (szint == 1)
			{
				arany = 100000;
				ezust = 105000;
				bronz = 110000;
				falak = new List<Fal>();
				falak.Add(new Fal(ActualWidth, ActualHeight, 1, 0, 0, true));
				falak.Add(new Fal(ActualWidth, ActualHeight, 1.5, 0, 90, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 2, 0, 90, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 2.5, 0, 90, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 3, 0, 90, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 3.5, 0, 90, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 4, 0, 90, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 4.5, 0, 90, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 5, 0, 90, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 5.5, 0, 90, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 6, 0, 90, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 6.5, 0, 90, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 7, 0, 90, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 2, 90, 180, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 2.5, 90, 180, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 3, 90, 180, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 3.5, 90, 180, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 4, 90, 180, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 4.5, 90, 180, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 5, 90, 180, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 5.5, 90, 180, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 6, 90, 180, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 6.5, 90, 180, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 7, 90, 180, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 7.5, 90, 180, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 1.5, 180, 270, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 2, 180, 270, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 2.5, 180, 270, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 3, 180, 270, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 3.5, 180, 270, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 4, 180, 270, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 4.5, 180, 270, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 5, 180, 270, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 5.5, 180, 270, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 6, 180, 270, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 6.5, 180, 270, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 7, 180, 270, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 1.5, 270, 0, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 2, 270, 0, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 2.5, 270, 0, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 3, 270, 0, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 3.5, 270, 0, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 4, 270, 0, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 4.5, 270, 0, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 5, 270, 0, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 5.5, 270, 0, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 6, 270, 0, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 6.5, 270, 0, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 7, 270, 0, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 7.5, 270, 0, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 8, 180, 180, true));
				falak.Add(new Fal(ActualWidth, ActualHeight, 8, 170, 190, false));
				pluszIdőMezo = new List<IMezo>();
				pluszIdőMezo.Add(new IMezo(ActualWidth, ActualHeight, 3, 340, 20, false));
				pluszIdőMezo.Add(new IMezo(ActualWidth, ActualHeight, 6, 350, 10, false));
				pluszIdőMezo.Add(new IMezo(ActualWidth, ActualHeight, 3, 70, 110, false));
				pluszIdőMezo.Add(new IMezo(ActualWidth, ActualHeight, 6, 80, 100, false));
				pluszIdőMezo.Add(new IMezo(ActualWidth, ActualHeight, 3, 160, 200, false));
				pluszIdőMezo.Add(new IMezo(ActualWidth, ActualHeight, 6, 170, 190, false));
				pluszIdőMezo.Add(new IMezo(ActualWidth, ActualHeight, 3, 250, 290, false));
				pluszIdőMezo.Add(new IMezo(ActualWidth, ActualHeight, 6, 260, 280, false));
				kapuNyito = null;
				jtk = new Jatekos(ActualWidth, ActualHeight);
				cel = new Fal(ActualWidth, ActualHeight, 1.5, 270, 270, false);
				kk = new Küszöbkör(ActualWidth, ActualHeight, 0);
				fmt = new FormattedText("Egyszerű pálya?\nNyomj Space-t a zöld területen\namikor az ütemjelző  a bal alsó sarokban\na középső fehér téglalapban van\n hogy visszaszorítsd az üldöző kört.", CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, new Typeface("Tahoma"), 12, Brushes.Black);
			}
			else if (szint == 2)
			{
				arany = 110000;
				ezust = 115000;
				bronz = 120000;
				falak = new List<Fal>();
				falak.Add(new Fal(ActualWidth, ActualHeight, 1, 0, 0, true));
				falak.Add(new Fal(ActualWidth, ActualHeight, 2, 180, 180, true));
				falak.Add(new Fal(ActualWidth, ActualHeight, 3, 90, 270, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 3, 270, 90, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 4, 180, 180, true));
				falak.Add(new Fal(ActualWidth, ActualHeight, 5, 0, 120, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 5, 120, 240, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 5, 240, 360, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 6, 30, 300, true));
				falak.Add(new Fal(ActualWidth, ActualHeight, 6, 300, 30, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 7, 0, 90, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 7, 90, 270, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 7, 270, 0, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 6.5, 0, 10, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 6.5, 80, 90, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 6.5, 160, 170, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 7.5, 230, 240, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 7.5, 350, 0, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 8, 180, 180, true));
				Fal kapu1 = new Fal(ActualWidth, ActualHeight, 2, 160, 200, false);
				falak.Add(kapu1);
				Fal kapu2 = new Fal(ActualWidth, ActualHeight, 3, 75, 105, false);
				falak.Add(kapu2);
				Fal kapu3 = new Fal(ActualWidth, ActualHeight, 3, 255, 285, false);
				falak.Add(kapu3);
				Fal kapu4 = new Fal(ActualWidth, ActualHeight, 7, 255, 285, false);
				falak.Add(kapu4);
				Fal kapu5 = new Fal(ActualWidth, ActualHeight, 7, 80, 100, false);
				falak.Add(kapu5);
				Fal kapu6 = new Fal(ActualWidth, ActualHeight, 6, 20, 40, false);
				falak.Add(kapu6);
				Fal kapu7 = new Fal(ActualWidth, ActualHeight, 5, 350, 10, false);
				falak.Add(kapu7);
				Fal kapu8 = new Fal(ActualWidth, ActualHeight, 5, 110, 130, false);
				falak.Add(kapu8);
				Fal kapu9 = new Fal(ActualWidth, ActualHeight, 5, 230, 250, false);
				falak.Add(kapu9);
				Fal kapu10 = new Fal(ActualWidth, ActualHeight, 7, 350, 10, false);
				falak.Add(kapu10);
				pluszIdőMezo = new List<IMezo>();
				pluszIdőMezo.Add(new IMezo(ActualWidth, ActualHeight, 2.5, 345, 15, false));
				pluszIdőMezo.Add(new IMezo(ActualWidth, ActualHeight, 2.5, 165, 195, false));
				pluszIdőMezo.Add(new IMezo(ActualWidth, ActualHeight, 6.5, 40, 50, false));
				kapuNyito = new List<IMezo>();
				kapuNyito.Add(new IMezo(ActualWidth, ActualHeight, 1.5, 333, 27, false));
				kapuNyito[0].Kapu = kapu1;
				kapuNyito.Add(new IMezo(ActualWidth, ActualHeight, 2.5, 75, 105, false));
				kapuNyito[1].Kapu = kapu3;
				kapuNyito.Add(new IMezo(ActualWidth, ActualHeight, 2.5, 255, 285, false));
				kapuNyito[2].Kapu = kapu2;
				kapuNyito.Add(new IMezo(ActualWidth, ActualHeight, 3.5, 260, 280, false));
				kapuNyito[3].Kapu = kapu4;
				kapuNyito.Add(new IMezo(ActualWidth, ActualHeight, 3.5, 80, 100, false));
				kapuNyito[4].Kapu = kapu5;
				kapuNyito.Add(new IMezo(ActualWidth, ActualHeight, 3.5, 350, 10, false));
				kapuNyito[5].Kapu = kapu6;
				kapuNyito.Add(new IMezo(ActualWidth, ActualHeight, 4.5, 352, 8, false));
				kapuNyito[6].Kapu = kapu7;
				kapuNyito.Add(new IMezo(ActualWidth, ActualHeight, 4.5, 112, 128, false));
				kapuNyito[7].Kapu = kapu8;
				kapuNyito.Add(new IMezo(ActualWidth, ActualHeight, 4.5, 232, 248, false));
				kapuNyito[8].Kapu = kapu9;
				kapuNyito.Add(new IMezo(ActualWidth, ActualHeight, 6.5, 70, 80, false));
				kapuNyito[9].Kapu = kapu10;
				jtk = new Jatekos(ActualWidth, ActualHeight);
				cel = new Fal(ActualWidth, ActualHeight, 8, 180, 180, false);
				kk = new Küszöbkör(ActualWidth, ActualHeight, 0);
				fmt = new FormattedText("Nyomj Space-t a kék területen \namikor az ütemjelző  a bal alsó sarokban\na középső fehér téglalapban van\nhogy kinyiss egyes falrészeket", CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, new Typeface("Tahoma"), 12, Brushes.Black);
			}
			else if (szint == 3)
			{
				arany = 100000;
				ezust = 105000;
				bronz = 110000;
				falak = new List<Fal>();
				falak.Add(new Fal(ActualWidth, ActualHeight, 1, 0, 0, true));
				falak.Add(new Fal(ActualWidth, ActualHeight, 2, 90, 270, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 2, 270, 90, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 3, 180, 0, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 3, 0, 180, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 4, 270, 90, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 4, 90, 270, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 5, 0, 120, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 5, 120, 240, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 5, 240, 360, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 6, 30, 150, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 6, 150, 30, true));
				falak.Add(new Fal(ActualWidth, ActualHeight, 7, 60, 150, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 7, 150, 240, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 7, 240, 330, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 7, 330, 60, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 8, 180, 180, true));
				falak.Add(new Fal(ActualWidth, ActualHeight, 2.5, 30, 60, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 2.5, 200, 230, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 3.5, 120, 140, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 3.5, 300, 320, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 4.5, 32, 48, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 4.5, 172, 188, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 5.5, 327, 340, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 5.5, 130, 142, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 6.5, 85, 95, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 6.5, 355, 5, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 6.5, 200, 212, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 7.5, 160, 170, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 7.5, 130, 140, false));
				falak.Add(new Fal(ActualWidth, ActualHeight, 7.5, 270, 280, false));
				Fal kapu1 = new Fal(ActualWidth, ActualHeight, 7, 230, 250, false);
				falak.Add(kapu1);
				Fal kapu2 = new Fal(ActualWidth, ActualHeight, 7, 50, 70, false);
				falak.Add(kapu2);
				pluszIdőMezo = new List<IMezo>();
				pluszIdőMezo.Add(new IMezo(ActualWidth, ActualHeight, 1.5, 330, 30, false));
				pluszIdőMezo.Add(new IMezo(ActualWidth, ActualHeight, 4.5, 80, 100, false));
				pluszIdőMezo.Add(new IMezo(ActualWidth, ActualHeight, 4.5, 260, 280, false));
				kapuNyito = new List<IMezo>();
				kapuNyito.Add(new IMezo(ActualWidth, ActualHeight, 5.5, 338, 352, false));
				kapuNyito[0].Kapu = kapu1;
				kapuNyito.Add(new IMezo(ActualWidth, ActualHeight, 5.5, 198, 212, false));
				kapuNyito[1].Kapu = kapu2;
				jtk = new Jatekos(ActualWidth, ActualHeight);
				cel = new Fal(ActualWidth, ActualHeight, 8, 180, 180, false);
				kk = new Küszöbkör(ActualWidth, ActualHeight, 0);
				fmt = new FormattedText("Utolsó pálya!", CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, new Typeface("Tahoma"), 12, Brushes.Black);
			}
		}

		/// <summary>
		/// Invalidate Visualok előtt
		/// </summary>
		public void Leptet()
		{
			if (jtk != null)
			{
				if (sw.ElapsedMilliseconds > 2000)
				{
					kk = new Küszöbkör(ActualWidth, ActualHeight, kk.R + 0.1); // 0.1
				}

				ft = new FormattedText((sw.ElapsedMilliseconds / 1000).ToString() + ":" + (sw.ElapsedMilliseconds % 1000).ToString(), CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, new Typeface("Tahoma"), 12, Brushes.Black);
				if (jtk.Utkozik(kk))
				{
					JatekVege(false);
				}

				if (jtk != null && jtk.Utkozik(cel))
				{
					szint++;
					JatekVege(true);
				}

				if (utemjelzok != null)
				{
					foreach (UtemJelzo a in utemjelzok)
					{
						a.Mozog();
					}
				}
			}
		}

		/// <summary>
		/// Imező Space lenyomásra történő vezérlése
		/// </summary>
		public void Interact()
		{
			bool utem = false;
			foreach (UtemJelzo a in utemjelzok)
			{
				if (Geometry.Combine(a.G.GetFlattenedPathGeometry(), utemCel.GetFlattenedPathGeometry(), GeometryCombineMode.Intersect, null).GetArea() > 0)
				{
					utem = true;
				}
			}

			if (jtk != null && utem)
			{
				if (pluszIdőMezo != null)
				{
					for (int i = 0; i < pluszIdőMezo.Count; i++)
					{
						if (jtk.Utkozik(pluszIdőMezo[i]) && !pluszIdőMezo[i].Ertek)
						{
							if (kk.R - 5 >= 0)
							{
								kk.R -= 10;
								pluszIdőMezo[i].Ertek = true;
							}
						}
					}
				}

				if (kapuNyito != null)
				{
					for (int i = 0; i < kapuNyito.Count; i++)
					{
						if (jtk.Utkozik(kapuNyito[i]) && !kapuNyito[i].Ertek)
						{
							falak.Remove(kapuNyito[i].Kapu);
							kapuNyito[i].Ertek = true;
						}
					}
				}
			}
		}

		/// <summary>
		/// Megjelenítő metódus
		/// </summary>
		/// <param name="drawingContext">generált bemenet</param>
		protected override void OnRender(DrawingContext drawingContext)
		{
			if (state == 1)
			{
				drawingContext.DrawRectangle(null, new Pen(Brushes.Black, 2), new Rect(20, ActualHeight - 60, 160, 40));
				if (utemCel != null)
				{
					drawingContext.DrawGeometry(Brushes.White, new Pen(Brushes.Black, 1), utemCel);
				}

				if (utemjelzok != null)
				{
					foreach (UtemJelzo a in utemjelzok)
					{
						drawingContext.DrawGeometry(Brushes.Black, new Pen(Brushes.Black, 2), a.G);
					}
				}

				if (cel != null)
				{
					drawingContext.DrawGeometry(Brushes.Yellow, null, cel.G);
				}

				if (kapuNyito != null)
				{
					foreach (Fal a in kapuNyito)
					{
						drawingContext.DrawGeometry(Brushes.Blue, new Pen(Brushes.Blue, 2.5), a.G);
					}
				}

				if (pluszIdőMezo != null)
				{
					foreach (Fal a in pluszIdőMezo)
					{
						drawingContext.DrawGeometry(Brushes.Green, new Pen(Brushes.Green, 2.5), a.G);
					}
				}

				if (jtk != null)
				{
					drawingContext.DrawGeometry(Brushes.Red, new Pen(Brushes.Black, 1), jtk.G);
				}

				if (kk != null)
				{
					drawingContext.DrawGeometry(new SolidColorBrush(Color.FromArgb(120, 255, 0, 0)), null, kk.G);
				}

				if (ft != null)
				{
					drawingContext.DrawGeometry(Brushes.Black, null, ft.BuildGeometry(new Point(10, 10)));
				}

				if (fmt != null)
				{
					drawingContext.DrawGeometry(Brushes.Black, null, fmt.BuildGeometry(new Point(ActualWidth - 250, 10)));
				}

				if (falak != null)
				{
					foreach (Fal a in falak)
					{
						drawingContext.DrawGeometry(Brushes.Black, new Pen(Brushes.Black, 3), a.G);
					}
				}
			}
			else if (state == 0)
			{
				if (r1 != null)
				{
					drawingContext.DrawRectangle(Brushes.Gray, new Pen(Brushes.Black, 2), r1);
				}

				if (ft != null)
				{
					drawingContext.DrawGeometry(Brushes.Black, null, ft.BuildGeometry(new Point(r1.Left + 10, (r1.Top + r1.Bottom - 40) / 2.0)));
				}

				if (r2 != null && (szint != 0 && szint < Szintszam))
				{
					drawingContext.DrawRectangle(Brushes.Gray, new Pen(Brushes.Black, 2), r2);
				}

				if (fmt != null && (szint != 0 && szint < Szintszam))
				{
					drawingContext.DrawGeometry(Brushes.Black, null, fmt.BuildGeometry(new Point(r2.Left + 10, (r2.Top + r2.Bottom - 40) / 2.0)));
				}
			}
			else if (state == 2)
			{
				if (r1 != null && szint != Szintszam)
				{
					drawingContext.DrawRectangle(Brushes.Gray, new Pen(Brushes.Black, 2), r1);
				}

				if (em != null && em.Szin == 0)
				{
					drawingContext.DrawGeometry(Brushes.Gold, new Pen(Brushes.Black, 2), em.G);
				}

				if (em != null && em.Szin == 1)
				{
					drawingContext.DrawGeometry(Brushes.Silver, new Pen(Brushes.Black, 2), em.G);
				}

				if (em != null && em.Szin == 2)
				{
					drawingContext.DrawGeometry(Brushes.Brown, new Pen(Brushes.Black, 2), em.G);
				}

				if (em != null && em.Szin == 3)
				{
					drawingContext.DrawGeometry(Brushes.White, new Pen(Brushes.Black, 2), em.G);
				}

				if (em != null)
				{
					drawingContext.DrawEllipse(Brushes.White, new Pen(Brushes.Black, 2), new Point(ActualWidth / 2.0, ActualHeight / 2.0 - 200), 20, 20);
				}

				if (ft != null)
				{
					drawingContext.DrawGeometry(Brushes.Black, null, ft.BuildGeometry(new Point(r1.Left + 10, (r1.Top + r1.Bottom - 40) / 2.0)));
				}

				if (r2 != null)
				{
					drawingContext.DrawRectangle(Brushes.Gray, new Pen(Brushes.Black, 2), r2);
				}

				FormattedText temp = new FormattedText("Vissza a menübe", CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, new Typeface("Tahoma"), 12, Brushes.Black);
				temp.SetFontSize(32);
				drawingContext.DrawGeometry(Brushes.Black, null, temp.BuildGeometry(new Point(r2.Left + 10, (r2.Top + r2.Bottom - 40) / 2.0)));
				if (fmt != null)
				{
					drawingContext.DrawGeometry(Brushes.Black, null, fmt.BuildGeometry(new Point(ActualWidth / 2.0 - 60, 10)));
				}
			}
		}

		/// <summary>
		/// r1,r2,uticel inicializálás dt tick feliratkozás mouse click feliratkozás
		/// </summary>
		/// <param name="sender">generált bemenet</param>
		/// <param name="e">másik generált bemenet</param>
		private void Jatek_Loaded(object sender, RoutedEventArgs e)
		{
			r1 = new Rect(ActualWidth / 2.0 - 200, ActualHeight / 2.0 - 60, 400, 60);
			r2 = new Rect(ActualWidth / 2.0 - 200, ActualHeight / 2.0 + 60, 400, 60);
			utemCel = new RectangleGeometry(new Rect(100, ActualHeight - 60, 20, 40));
			dt.Interval = TimeSpan.FromMilliseconds(20);
			dt.Tick += Dt_Tick;
			MouseLeftButtonDown += Jatek_MouseLeftButtonDown;
			dt.Start();
		}

		/// <summary>
		/// click esemeny 
		/// </summary>
		/// <param name="sender">generált bemenet</param>
		/// <param name="e">másik generált bemenet</param>
		private void Jatek_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			if (r1.Contains(e.GetPosition(this)))
			{
				if (state == 0)
				{
					szint = 0;
					StateChange(1);
				}
				else if (state == 2)
				{
					StateChange(1);
				}
			}

			if (r2.Contains(e.GetPosition(this)))
			{
				if (state == 0)
				{
					StateChange(1);
				}
				else if (state == 2)
				{
					StateChange(0);
				}
			}
		}
	}
}
