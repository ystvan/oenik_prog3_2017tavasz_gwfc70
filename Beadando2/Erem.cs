﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Beadando2
{
	/// <summary>
	/// Erem osztály
	/// </summary>
	public class Erem : Jatekelem
	{
		/// <summary>
		/// Érem tipus
		/// </summary>
		private int szin;

		/// <summary>
		/// Initializes a new instance of the <see cref="Erem"/> class.
		/// </summary>
		/// <param name="aw">Actual Width</param>
		/// <param name="ah">Actual Heigth</param>
		public Erem(double aw, double ah)
		{
			Geometry temp;
			KözepPont = new Point(aw / 2.0, ah / 2.0 - 200);
			EllipseGeometry eg1 = new EllipseGeometry(new Point(-10, -10), 25, 25);
			EllipseGeometry eg2 = new EllipseGeometry(new Point(0, -10), 25, 25);
			EllipseGeometry eg3 = new EllipseGeometry(new Point(10, -10), 25, 25);
			EllipseGeometry eg4 = new EllipseGeometry(new Point(10, 0), 25, 25);
			EllipseGeometry eg5 = new EllipseGeometry(new Point(10, 10), 25, 25);
			EllipseGeometry eg6 = new EllipseGeometry(new Point(0, 10), 25, 25);
			EllipseGeometry eg7 = new EllipseGeometry(new Point(-10, 10), 25, 25);
			EllipseGeometry eg8 = new EllipseGeometry(new Point(-10, 0), 25, 25);
			temp = Geometry.Combine(eg1, eg2, GeometryCombineMode.Union, null);
			temp = Geometry.Combine(temp, eg3, GeometryCombineMode.Union, null);
			temp = Geometry.Combine(temp, eg4, GeometryCombineMode.Union, null);
			temp = Geometry.Combine(temp, eg5, GeometryCombineMode.Union, null);
			temp = Geometry.Combine(temp, eg6, GeometryCombineMode.Union, null);
			temp = Geometry.Combine(temp, eg7, GeometryCombineMode.Union, null);
			temp = Geometry.Combine(temp, eg8, GeometryCombineMode.Union, null);
			G = temp;
		}

		/// <summary>
		/// Gets or sets szin
		/// </summary>
		public int Szin
		{
			get
			{
				return szin;
			}

			set
			{
				szin = value;
			}
		}
	}
}
