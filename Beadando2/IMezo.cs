﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beadando2
{
	/// <summary>
	/// Interaktív mező osztály
	/// </summary>
	public class IMezo : Fal
	{
		/// <summary>
		/// Használt e
		/// </summary>
		private bool ertek;

		/// <summary>
		/// Nyitás referencia
		/// </summary>
		private Fal kapu;

		/// <summary>
		/// Initializes a new instance of the <see cref="IMezo"/> class.
		/// </summary>
		/// <param name="aw">Actual Width</param>
		/// <param name="ah">Actual Heigth</param>
		/// <param name="szint">Labirintus szint</param>
		/// <param name="sSzog">Kezdő szög</param>
		/// <param name="vSzog">Vég szög</param>
		/// <param name="largeArc">Large arc</param>
		public IMezo(double aw, double ah, double szint, double sSzog, double vSzog, bool largeArc) : base(aw, ah, szint, sSzog, vSzog, largeArc)
		{
		}

		/// <summary>
		/// Gets or sets a value indicating whether ertek is true or false
		/// </summary>
		public bool Ertek
		{
			get
			{
				return ertek;
			}

			set
			{
				ertek = value;
			}
		}

		/// <summary>
		/// Gets or sets kapu
		/// </summary>
		public Fal Kapu
		{
			get
			{
				return kapu;
			}

			set
			{
				kapu = value;
			}
		}
	}
}
