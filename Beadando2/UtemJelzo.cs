﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Beadando2
{
	/// <summary>
	/// UtemJelzo osztály
	/// </summary>
	public class UtemJelzo : Jatekelem
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="UtemJelzo"/> class.
		/// </summary>
		/// <param name="aw">Actual Width</param>
		/// <param name="ah">Actual Height</param>
		/// <param name="xset">Eltolás érték</param>
		public UtemJelzo(double aw, double ah, double xset)
		{
			KözepPont = new Point(100 + xset, ah - 40);
			PathGeometry pg = new PathGeometry();
			PathFigure pf = new PathFigure();
			pf.StartPoint = new Point(0, -20);
			PathSegmentCollection psc = new PathSegmentCollection();
			psc.Add(new LineSegment(new Point(0, 20), true));
			psc.Add(new LineSegment(new Point(5, 20), true));
			psc.Add(new LineSegment(new Point(5, -20), true));
			psc.Add(new LineSegment(new Point(0, -20), true));
			pf.Segments = psc;
			PathFigureCollection pfc = new PathFigureCollection();
			pfc.Add(pf);
			pg.Figures = pfc;
			G = pg;
		}

		/// <summary>
		/// Utemjelzo Mozgás metódus
		/// </summary>
		public void Mozog()
		{
			Point temp = KözepPont;
			temp.X = KözepPont.X - 2;
			if (KözepPont.X <= 20)
			{
				temp.X = 180;
			}

			KözepPont = temp;
		}
	}
}
