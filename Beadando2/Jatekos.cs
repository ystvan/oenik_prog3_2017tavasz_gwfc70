﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Beadando2
{
	/// <summary>
	/// JAtekos osztály
	/// </summary>
	public class Jatekos : Jatekelem
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="Jatekos"/> class.
		/// </summary>
		/// <param name="aw">Actual Width</param>
		/// <param name="ah">Actual Height</param>
		public Jatekos(double aw, double ah)
		{
			KözepPont = new Point(aw / 2.0, ah / 2.0);
			double a = 2 * M / Math.Sqrt(3.0);
			PathGeometry pg = new PathGeometry();
			PathFigure pf = new PathFigure();
			pf.StartPoint = new Point(0, -(M - a / 4));
			PathSegmentCollection psc = new PathSegmentCollection();
			psc.Add(new LineSegment(new Point(a / 2, a / 4), true));
			psc.Add(new LineSegment(new Point(-a / 2, a / 4), true));
			psc.Add(new LineSegment(new Point(0, -(M - a / 4)), true));
			pf.Segments = psc;
			PathFigureCollection pfc = new PathFigureCollection();
			pfc.Add(pf);
			pg.Figures = pfc;
			G = pg;
		}

		/// <summary>
		/// Játékos mozgás metódus
		/// </summary>
		/// <param name="irany">előre vagy hátra</param>
		/// <returns>Régi középpont ütközésviszgálathoz</returns>
		public Point Mozog(bool irany)
		{
			Point temp = KözepPont;
			if (irany)
			{
				KözepPont = new Point(KözepPont.X + 2 * Math.Sin(Szog * (Math.PI / 180)), KözepPont.Y - 2 * Math.Cos(Szog * (Math.PI / 180)));
			}
			else if (!irany)
			{
				KözepPont = new Point(KözepPont.X - 2 * Math.Sin(Szog * (Math.PI / 180)), KözepPont.Y + 2 * Math.Cos(Szog * (Math.PI / 180)));
			}

			return temp;
		}

		/// <summary>
		/// Játékos forgulásmetódus
		/// </summary>
		/// <param name="aw">Actual Width</param>
		/// <param name="ah">Actual Heigth</param>
		/// <param name="irany">jobbra vagy balra</param>
		/// <returns>Régi középpont ütközésviszgálathoz</returns>
		public Point Fordul(double aw, double ah, bool irany)
		{
			Point temp = KözepPont;
			double r = Math.Sqrt(Math.Pow(aw / 2.0 - KözepPont.X, 2) + Math.Pow(ah / 2.0 - KözepPont.Y, 2));
			if (irany)
			{
				Szog += 2;
				KözepPont = new Point(aw / 2.0 + r * Math.Sin(Szog * (Math.PI / 180)), ah / 2.0 - r * Math.Cos(Szog * (Math.PI / 180)));
			}
			else if (!irany)
			{
				Szog -= 2;
				KözepPont = new Point(aw / 2.0 + r * Math.Sin(Szog * (Math.PI / 180)), ah / 2.0 - r * Math.Cos(Szog * (Math.PI / 180)));
			}

			return temp;
		}
	}
}
