﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Beadando2
{
	/// <summary>
	/// Fal osztály
	/// </summary>
	public class Fal:Jatekelem
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="Fal"/> class.
		/// </summary>
		/// <param name="aw">Actual Width</param>
		/// <param name="ah">Actual Height</param>
		/// <param name="szint">Labirintus szint</param>
		/// <param name="sSzog">Kezdő szög</param>
		/// <param name="vSzog">Vég szög</param>
		/// <param name="largeArc">Large arc</param>
		public Fal(double aw, double ah, double szint, double sSzog, double vSzog, bool largeArc)
		{
			KözepPont = new Point(aw / 2.0, ah / 2.0);
			double r = szint * (M + 1) * 2;
			double t = Math.Sqrt(Math.Pow(r, 2) - Math.Pow(12, 2));
			double[] szogRad = new double[2];
			szogRad[0] = sSzog * (Math.PI / 180);
			szogRad[1] = vSzog * (Math.PI / 180);
			PathGeometry pg = new PathGeometry();

			PathFigureCollection pfc = new PathFigureCollection();
			PathFigure pf = new PathFigure();
			pf.StartPoint = new Point((Math.Round((12 * Math.Sin((Math.PI / 2) - szogRad[0])), 3) + t * Math.Sin(szogRad[0])), (12 * Math.Cos((Math.PI / 2) - szogRad[0]) - t * Math.Cos(szogRad[0])));
			PathSegmentCollection psc = new PathSegmentCollection();
			psc.Add(new ArcSegment(new Point(-Math.Round((12 * Math.Sin((Math.PI / 2) - szogRad[1])), 3) + t * Math.Sin(szogRad[1]), (-12 * Math.Cos((Math.PI / 2) - szogRad[1]) - t * Math.Cos(szogRad[1]))), new Size(r, r), 0, largeArc, SweepDirection.Clockwise, true));
			psc.Add(new LineSegment(new Point(-Math.Round((12 * Math.Sin((Math.PI / 2) - szogRad[1])), 3) + (t - 20) * Math.Sin(szogRad[1]), (-12 * Math.Cos((Math.PI / 2) - szogRad[1]) - (t - 20) * Math.Cos(szogRad[1]))), true));
			psc.Add(new ArcSegment(new Point((Math.Round((12 * Math.Sin((Math.PI / 2) - szogRad[0])), 3) + (t - 20) * Math.Sin(szogRad[0])), (12 * Math.Cos((Math.PI / 2) - szogRad[0]) - (t - 20) * Math.Cos(szogRad[0]))), new Size(r - 20, r - 20), 0, largeArc, SweepDirection.Counterclockwise, true));
			psc.Add(new LineSegment(new Point((Math.Round((12 * Math.Sin((Math.PI / 2) - szogRad[0])), 3) + t * Math.Sin(szogRad[0])), (12 * Math.Cos((Math.PI / 2) - szogRad[0]) - t * Math.Cos(szogRad[0]))), true));
			pf.Segments = psc;
			pfc.Add(pf);
			pg.Figures = pfc;
			G = pg;
		}
	}
}
