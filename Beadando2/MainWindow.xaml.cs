﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Beadando2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
		/// <summary>
		/// Initializes a new instance of the <see cref="MainWindow"/> class.
		/// </summary>
		public MainWindow()
        {
            InitializeComponent();
			KeyDown += MainWindow_KeyDown;
		}
		/// <summary>
		/// Key down esemény kezelés
		/// </summary>
		/// <param name="sender">generált bemenet</param>
		/// <param name="e">másik generált bemenet</param>
		private void MainWindow_KeyDown(object sender, KeyEventArgs e)
		{
			switch (e.Key)
			{
				case Key.Up:
					(Content as Jatek).Mozog(true);
					break;
				case Key.Down:
					(Content as Jatek).Mozog(false);
					break;
				case Key.Right:
					(Content as Jatek).Fordul(true);
					break;
				case Key.Left:
					(Content as Jatek).Fordul(false);
					break;
				case Key.Space:
					(Content as Jatek).Interact();
					break;
			}
		}
	}
}
